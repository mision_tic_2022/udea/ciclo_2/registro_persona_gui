package com.registro_persona.controlador;

import java.util.ArrayList;

import com.registro_persona.modelo.Persona;

public class PersonaController {
    //ATRIBUTO
    //Representa un arreglo de tamaño dinámico
    private ArrayList<Persona> personas;

    //CONSTRUCTOR
    public PersonaController(){
        this.personas = new ArrayList<Persona>();
    }
    
    //COSULTORES
    public Persona getPersona(int pos){
        return personas.get(pos);
    }

    public int cantPeronas(){
        return personas.size();
    }

    //MODIFICADORES
    public void setPersona(Persona persona, int pos){
        personas.set(pos, persona);
    }

    //ACCIONES
    public void registrarPersona(String nombre, String apellido, int edad, String cedula, char sexo){
        //Crear objeto Persona
        Persona persona = new Persona(nombre, apellido, edad, cedula, sexo);
        //Añadir al arreglo
        personas.add(persona);
    }

    public void modificarPersona(String nombre, String apellido, int edad, String cedula, char sexo){
        int index = buscarXCedula(cedula);
        getPersona(index).setNombre(nombre);
        getPersona(index).setApellido(apellido);
        getPersona(index).setSexo(sexo);
        getPersona(index).setEdad(edad);
    }

    /**
     * 
     * @param cedula
     * Retorna el indice en el que se encuentra el objeto,
     * de no existir retorna -1
     */
    public int buscarXCedula(String cedula){
        int index = -1;
        for(int i = 0; i < cantPeronas(); i++){
            Persona persona = personas.get(i);
            if(persona.getCedula().equals(cedula)){
                index = i;
                break;
            }
        }
        return index;
    }

}
