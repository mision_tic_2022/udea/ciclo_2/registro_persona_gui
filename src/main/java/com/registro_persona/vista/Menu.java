package com.registro_persona.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.registro_persona.controlador.PersonaController;

//Importar librería manejadora de eventos de la GUI
import java.awt.event.*;

public class Menu extends JFrame {

    // ATRIBUTOS
    private PersonaController controller;
    private JButton btnRegistro;
    private JButton btnVerInfo;
    private JButton btnInfoXCedula;
    private JButton btnEliminar;
    private JButton btnSalir;

    // CONSTRUCTOR
    public Menu() {
        controller = new PersonaController();
        // Configuración de la ventana (JFrame)
        this.setTitle("Menu");
        // Indicar que finalice el programa cuando el usuario dé click en la equis 'X'
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);
        this.setBounds(50, 50, 400, 250);

        // INICIALIZAR ATRIBUTOS
        btnRegistro = new JButton("Registrar persona");
        btnRegistro.setBounds(30, 30, 150, 30);
        this.add(btnRegistro);

        btnVerInfo = new JButton("Mostrar información");
        btnVerInfo.setBounds(180, 30, 160, 30);
        this.add(btnVerInfo);

        btnInfoXCedula = new JButton("Consultar por cédula");
        btnInfoXCedula.setBounds(30, 70, 180, 30);
        this.add(btnInfoXCedula);

        btnEliminar = new JButton("Eliminar persona");
        btnEliminar.setBounds(210, 70, 160, 30);
        this.add(btnEliminar);

        btnSalir = new JButton("Salir");
        btnSalir.setBounds(150, 140, 100, 40);
        this.add(btnSalir);

        // AÑADIR MANEJADORES DE EVENTOS
        manejadorEventoBtnRegistro();
        manejadorEventoBtnVerInfo();
        manejadorEventoBtnInfoXCedula();
        manejadorEventoBtnSalir();

        // Poner visible la ventana
        this.setVisible(true);
    }

    public void manejadorEventoBtnRegistro() {
        btnRegistro.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                Formulario formulario = new Formulario(controller, "Resgistro Persona");
            }
        });
    }

    public void manejadorEventoBtnVerInfo() {
        btnVerInfo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                mostrarInfoPersonas();
            }
        });
    }

    public void manejadorEventoBtnSalir(){
        btnSalir.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                dispose();
            }
        });
    }

    public void mostrarInfoPersonas(){
        String info = "";
        for(int i = 0; i < controller.cantPeronas(); i++){
            info += controller.getPersona(i);
        }
        if(info.isEmpty()){
            info = "No hay personas registradas";
        }
        JOptionPane.showMessageDialog(this, info);
    }

    public void manejadorEventoBtnInfoXCedula(){
        btnInfoXCedula.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                consultarPersona();
            }
        });
    }

    public void consultarPersona(){
        //Formulario formulario = new Formulario(controller, "Andres", "Quintero", 20, 'M', "123456", "Modificar Persona");
        String cedula = JOptionPane.showInputDialog(this, "Por favor ingrese la cédula de la persona a consultar");
        int index = controller.buscarXCedula(cedula);
        if(index > -1){
            Formulario formulario = new Formulario(controller, controller.getPersona(index).getNombre(), controller.getPersona(index).getApellido(), controller.getPersona(index).getEdad(), controller.getPersona(index).getSexo(), cedula, "Modificar Persona");
        }else{
            JOptionPane.showMessageDialog(this, "La cédula ingresada no se encuentra en la base de datos");
        }
    }

}
