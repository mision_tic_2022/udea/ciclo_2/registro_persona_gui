package com.registro_persona.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.registro_persona.controlador.PersonaController;

import java.awt.event.*;

public class Formulario extends JFrame {
    //Atributos
    private PersonaController controller;
    private boolean registrar;
    //ATRIBUTOS GUI
    private JLabel lblNombre;
    private JLabel lblApellido;
    private JLabel lblEdad;
    private JLabel lblSexo;
    private JLabel lblCedula;
    private JTextField campoNombre;
    private JTextField campoApellido;
    private JTextField campoEdad;
    private JTextField campoSexo;
    private JTextField campoCedula;
    private JButton btnRegistrar;
    private JButton btnSalir;

    //CONSTRUCTOR
    public Formulario(PersonaController controller, String tituloVentana){
        this.controller = controller;
        this.registrar = true;
        construirUI(tituloVentana);
    }

    public Formulario(PersonaController controller, String nombre, String apellido, int edad, char sexo, String cedula, String tituloVentana){
        this.controller = controller;
        this.registrar = false;
        construirUI(tituloVentana);
        asignarValores(nombre, apellido, edad, sexo, cedula);
    }

    public void construirUI(String tituloVentana){
        //Titulo
        this.setTitle(tituloVentana);
        //Eliminar el flowLayout que trae por defecto para manejar el esquema por coordenadas
        this.getContentPane().setLayout(null);
        //Dar tamaño y ubicación a la ventana
        this.setBounds(100, 50, 320, 340);

        //Inicializar los atributos
        lblNombre = new JLabel("Nombre: ");
        lblNombre.setBounds(20, 20, 90, 30);
        this.add(lblNombre);

        campoNombre = new JTextField();
        campoNombre.setBounds(100, 20, 80, 30);
        this.add(campoNombre);

        lblApellido = new JLabel("Apellido: ");
        lblApellido.setBounds(20, 60, 80, 30);
        this.add(lblApellido);

        campoApellido = new JTextField();
        campoApellido.setBounds(100, 60, 80, 30);
        this.add(campoApellido);

        lblEdad = new JLabel("Edad: ");
        lblEdad.setBounds(20, 100, 80, 30);
        this.add(lblEdad);

        campoEdad = new JTextField();
        campoEdad.setBounds(100, 100, 80, 30);
        this.add(campoEdad);

        lblSexo = new JLabel("Sexo: ");
        lblSexo.setBounds(20, 140, 80, 30);
        this.add(lblSexo);

        campoSexo = new JTextField();
        campoSexo.setBounds(100, 140, 80, 30);
        this.add(campoSexo);

        lblCedula = new JLabel("Cédula: ");
        lblCedula.setBounds(20, 180, 80, 30);
        this.add(lblCedula);

        campoCedula = new JTextField();
        campoCedula.setBounds(100, 180, 80, 30);
        this.add(campoCedula);
        
        //Operador ternario
        //Si se cumple la condición se asigna lo que está después del '?' sino lo que está después del ':'
        String txtBtn = registrar ? "Registrar" : "Modificar";
        btnRegistrar = new JButton(txtBtn);
        btnRegistrar.setBounds(180, 230, 100, 50);
        this.add(btnRegistrar);


        btnSalir = new JButton("Cerrar");
        btnSalir.setBounds(50, 230, 100, 50);
        this.add(btnSalir);

        //MANEJADOR DE EVENTOS
        manejadorEventosBtnRegistrar();
        manejadorEventoBtnSalir();
        
        this.setVisible(true);
    }

    public void manejadorEventosBtnRegistrar(){
        btnRegistrar.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent event){
                //String nombre, String apellido, int edad, String cedula, char sexo
                String nombre = campoNombre.getText();
                String apellido = campoApellido.getText();
                String strEdad = campoEdad.getText();
                String cedula = campoCedula.getText();
                String strSexo = campoSexo.getText();
                //Validar
                if(!nombre.isEmpty() && !apellido.isEmpty() && !strEdad.isEmpty() 
                    && !cedula.isEmpty() && !strSexo.isEmpty()){
                        int edad = Integer.parseInt( strEdad );
                        char sexo = strSexo.charAt(0);
                        if(registrar){
                            controller.registrarPersona(nombre, apellido, edad, cedula, sexo);
                            limpiarCampos();
                        }else{
                            controller.modificarPersona(nombre, apellido, edad, cedula, sexo);
                            cerrarVentana();
                        }
                        JOptionPane.showMessageDialog(null, "Acción realizada con éxito");
                }else{
                    JOptionPane.showMessageDialog(null, "Por favor llene todos los campos");
                }
            }
        } );
    }

    public void manejadorEventoBtnSalir(){
        btnSalir.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                cerrarVentana();
            }
        });
    }


    public void limpiarCampos(){
        campoNombre.setText("");
        campoApellido.setText("");
        campoEdad.setText("");
        campoCedula.setText("");
        campoSexo.setText("");
    }

    public void asignarValores(String nombre, String apellido, int edad, char sexo, String cedula){
        campoNombre.setText(nombre);
        campoApellido.setText(apellido);
        campoEdad.setText(""+edad);
        campoCedula.setText(cedula);
        campoSexo.setText(""+sexo);
        if(!registrar){
            campoCedula.setEditable(false);
        }
    }

    public void cerrarVentana(){
        this.dispose();
    }

}
